#!/bin/bash

## Check existence of zh-Hanc.txt
if [ -s zh-Hanc.txt ]; then
	echo "3e3e058cdca27afc1690536409622a2d46427999fdca7db4dc011406a0ab9d83  zh-Hanc.txt" | shasum -a256 -c
	exit 0
fi

## kSimplifiedVariant.txt L3024
wget --no-verbose https://www.unicode.org/Public/UNIDATA/Unihan.zip
unzip Unihan.zip Unihan_Variants.txt && rm Unihan.zip
sed '/^#/d;/^$/d' Unihan_Variants.txt | grep "kSimplifiedVariant\tU" | sed $'s/^.*Variant\t//' | sed $'s/ /\\\n/g' | sort | uniq > kSimplifiedVariant.txt && rm Unihan_Variants.txt
echo

## eduSong L25512
wget --no-verbose https://language.moe.gov.tw/001/Upload/Files/site_content/M0001/eduSong_Unicode.zip
unzip eduSong_Unicode.zip && rm eduSong_Unicode.zip
otfinfo -u eduSong_Unicode.ttf | sed 's/ .*$//;s/uni/U+/' > eduSong.txt && rm eduSong_Unicode.ttf
echo

## zh-Hanc L2508
awk 'NR==FNR{a[$0]=1;next}!a[$0]' eduSong.txt kSimplifiedVariant.txt > zh-Hanc.txt && rm eduSong.txt kSimplifiedVariant.txt


