#!/bin/bash

if [[ $# -eq 0 ]] ; then
	echo -e '\nUsage: ./rm-zhc.sh file.ttf\n'
	exit 0
fi

FONT=$(basename $1)
otfinfo -u $1 | sed 's/ .*$//;s/uni/U+/' > unicode.txt
while read han; do sed -i '' "/$han/d" unicode.txt; done <zh-Hanc.txt
pyftsubset $1 --unicodes-file=unicode.txt --output-file=${FONT/.ttf}-no-zhc.ttf
rm unicode.txt

