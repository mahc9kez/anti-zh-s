# ANTI zh-Hans/zh-Hanc

Remove zh-Hanc from fonts.

**zh-Hanc:** 殘體字, Simplified Chinese characters, _c_ for 殘 means incomplete/disabled/deform. It's refering to these Chinese characters which simplificated and standardized by the government of PRC  
**zh-Hans:** Simplified Chinese

This project give you an option to fully unsee these Simplified Chinese characters, if you prefer to see "Tofu" rather than these ugly/stupid zh-Hanc, this would be exactly what you want.

## Dependencies

`otfinfo` from [lcdf-typetools](https://www.lcdf.org/type/)  
`pyftsubset` from [fonttools](https://github.com/fonttools/fonttools)

## Usage

The list of zh-Hanc Unicode code point is located at font/zh-Hanc.txt, run this command if you want to generate it by yourself.  
`cd unicode/ && ./genlist-zhc.sh`  
To check the generated **zh-Hanc.txt** (it should contains 2508 lines), simply just run `./genlist-zhc.sh` again.

Generate font without zh-Hanc from existing TTF  
`cd font/ && ./rm-zhc.sh file.ttf`
